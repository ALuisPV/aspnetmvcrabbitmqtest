﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitWorker
{
    class Worker
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                // Cola declarara como durable - Si RabbitMQ peta, la cola seguira
                channel.QueueDeclare(queue: "task_queue",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                // QoS para Workers en RabbitMQ - RabbitMQ no pasara nuevos mensajes al Worker hasta que no haya enviado el ACK del anterior
                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);

                    // Fake Action: Ejecutar la tarea
                    int dots = message.Split('.').Length - 1;
                    System.Threading.Thread.Sleep(1000 * dots);

                    // Enviar ACK Basico a RabbitMQ - Hemos ejecutado la tarea. Así, RabbitMQ descarta el mensaje y no lo encola de nuevo
                    // OJO: Error Comun: Si trabajamos con ACKs, NO OLVIDAR ESTO. Si no, RabbitMQ estara sirviendo el mismo mensaje siempre
                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                    Console.WriteLine(" [x] ACK Enviado. Tarea realizada");
                };

                channel.BasicConsume(queue: "task_queue",
                                     noAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();

            }
        }

    }
}
