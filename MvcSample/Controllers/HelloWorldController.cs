﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace MvcSample.Controllers
{
    public class HelloWorldController : Controller
    {
        // 
        // GET: /HelloWorld/ 

        public ActionResult Index()
        {
            return View();
        }

        // 
        // GET: /HelloWorld/Welcome/ 

        public ActionResult Welcome(string name, int ID = 1)
        {
            ViewBag.mensaje = "Hello " + name;
            ViewBag.numTimes = ID;
            return View();
        }

        //
        // GET: /HelloWorld/RabbitSend/

        public ActionResult RabbitSend(string name, int ID = 1)
        {
            // Factoria de Conexiones a un Host RabbitMQ
            var factory = new ConnectionFactory() { HostName = "localhost" };
            // Crear una conexion a RabbitMQ
            using (var connection = factory.CreateConnection())
            // Crear un canal sobre la conexion (Modelo)
            using (var channel = connection.CreateModel())
            {
                // Declarar la cola
                channel.QueueDeclare(queue: "hello",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
                
                // Preparar datos (cadena)
                var body = Encoding.UTF8.GetBytes(name);

                // Publicar datos en la cola
                channel.BasicPublish(exchange: "",
                    routingKey: "hello",
                    basicProperties: null,
                    body: body);
            }

            ViewBag.mensaje = "Mensaje Enviado: " + name;
            return View();
        }
        //
        // GET: /HelloWorld/RabbitNewTask/

        public ActionResult RabbitNewTask(string name, int ID = 1)
        {
            // Fake Task
            name = "Tarea....";
            // Factoria de Conexiones a un Host RabbitMQ
            var factory = new ConnectionFactory() { HostName = "localhost" };
            // Crear una conexion a RabbitMQ
            using (var connection = factory.CreateConnection())
            // Crear un canal sobre la conexion (Modelo)
            using (var channel = connection.CreateModel())
            {
                // Declarar la cola como durable: Hara que, si RabbitMQ peta, la cola permanezca a su regreso
                channel.QueueDeclare(queue: "task_queue",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                // Preparar datos (cadena)
                var body = Encoding.UTF8.GetBytes(name);

                // Configuramos persistencia
                // Con esto conseguimos que nuestros mensajes se pasen a DISCO
                // Hay un Lapsus de tiempo entre que se recibe el mensaje en RabbitMQ y este es guardado
                // Si RabbitMQ peta EN ESE MOMENTO, puede perderse el mensaje. Habria que recurrir a otros metodos para garantizar persistencia
                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                // Publicar datos en la cola (y asociamos las propiedades)
                channel.BasicPublish(exchange: "",
                    routingKey: "task_queue",
                    basicProperties: properties,
                    body: body);
            }

            ViewBag.mensaje = "Tarea Enviada: " + name;
            return View();
        }

        //
        // GET: /HelloWorld/RabbitExchange/

        public ActionResult RabbitExchange(string name, int ID = 1)
        {
            // Fake Log
            name = "Log 1";
            // Factoria de Conexiones a un Host RabbitMQ
            var factory = new ConnectionFactory() { HostName = "localhost" };
            // Crear una conexion a RabbitMQ
            using (var connection = factory.CreateConnection())
            // Crear un canal sobre la conexion (Modelo)
            using (var channel = connection.CreateModel())
            {
                // Declarar el Exchange al cual enviar los Logs. Exchange de tipo "Fanout" (enviara msgs a todas las colas que conozca)
                // OJO: Aqui NO DECLARAMOS colas. Solo un Exchange al cual enviamos datos.
                //      Seran los consumidores los que se encarguen de gestionar colas "nuevas", segun el exchange, para recibir datos
                channel.ExchangeDeclare(exchange: "logs", type: "fanout");
                
                // Preparar datos (cadena)
                var body = Encoding.UTF8.GetBytes(name);

                // Publicar datos en la cola (y asociamos las propiedades)
                channel.BasicPublish(exchange: "logs",
                    routingKey: "",
                    basicProperties: null,
                    body: body);
            }

            ViewBag.mensaje = "Log Enviado: " + name;
            return View();
        }

        //
        // GET: /HelloWorld/RabbitRecv/

        public ActionResult RabbitRecv()
        {

            string cadena = "VACIO";
            // Factoria de Conexiones a un Host RabbitMQ
            var factory = new ConnectionFactory() { HostName = "localhost" };
            // Crear una conexion a RabbitMQ
            using (var connection = factory.CreateConnection())
            // Crear un canal sobre la conexion (Modelo)
            using (var channel = connection.CreateModel())
            {
                // Declarar la cola y los parámetros de la misma
                channel.QueueDeclare(queue: "hello",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                // Gestor de eventos para consumir
                var consumer = new EventingBasicConsumer(channel);

                // Añadir metodo a ejecutar cuando se dispare el consumo
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    cadena = message;
                };

                // Disparar el consumo, indicando QUIEN es el gestor que se hace cargo de este consumo (consumer)
                channel.BasicConsume(queue: "hello",
                                     noAck: true,
                                     consumer: consumer);
                System.Threading.Thread.Sleep(2000);
            }

            ViewBag.mensaje = "Mensaje Consumido: " + cadena;
            return View();
        }
    }
}