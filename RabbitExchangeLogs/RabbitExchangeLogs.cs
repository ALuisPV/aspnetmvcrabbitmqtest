﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitExchangeLogs
{
    class RabbitExchangeLogs
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                // Declaramos el Exchange de Logs
                channel.ExchangeDeclare(exchange: "logs", type: "fanout");

                // Creamos la cola (generica, con nombre aleatorio)
                var queueName = channel.QueueDeclare().QueueName;

                // Binding entre Cola y Exchange (MSG --> EXCHANGE --> [Q1, Q2, ... , QN])
                channel.QueueBind(queue: queueName,
                                  exchange: "logs",
                                  routingKey: "");

                // Evento de Consumo
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Log: {0}", message);
                };

                // Consumimos de la Cola con nombre aleatorio que creamos anteriormente
                channel.BasicConsume(queue: queueName,
                                     noAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();

            }
        }
    }
}
